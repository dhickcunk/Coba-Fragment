package com.dhickcunk.modul2_praktikum.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhickcunk.modul2_praktikum.MainActivity;
import com.dhickcunk.modul2_praktikum.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {
    public static MainActivity mainActivity;


    public static ContactFragment newInstance(MainActivity activity){
        mainActivity = activity;
        return new ContactFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.fragment_contact, container, false);
        return view;
    }

}
