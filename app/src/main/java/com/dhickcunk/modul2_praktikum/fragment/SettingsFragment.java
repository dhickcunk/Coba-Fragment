package com.dhickcunk.modul2_praktikum.fragment;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.dhickcunk.modul2_praktikum.LoginActivity;
import com.dhickcunk.modul2_praktikum.MainActivity;
import com.dhickcunk.modul2_praktikum.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    public static MainActivity mainActivity;
    private Button btnLogout;

    public static SettingsFragment newInstance(MainActivity activity){
        mainActivity = activity;
        return new SettingsFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.fragment_settings, container, false);

        btnLogout = (Button) view.findViewById(R.id.buttonLogout);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsFragment.this.getActivity(), LoginActivity.class);
                startActivity(intent);
                SettingsFragment.this.getActivity().finish();
            }
        });
    }
}
