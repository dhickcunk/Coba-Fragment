package com.dhickcunk.modul2_praktikum;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.dhickcunk.modul2_praktikum.fragment.ContactFragment;
import com.dhickcunk.modul2_praktikum.fragment.GalleryFragment;
import com.dhickcunk.modul2_praktikum.fragment.ProfileFragment;
import com.dhickcunk.modul2_praktikum.fragment.SettingsFragment;

public class MainActivity extends AppCompatActivity {
    private Button btnProfile;
    private Button btnGallery;
    private Button btnSetting;
    private Button btnContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        changeContent(ProfileFragment.newInstance(this));

        btnContact = (Button) findViewById(R.id.buttonContact);
        btnGallery = (Button) findViewById(R.id.buttonGallery);
        btnSetting = (Button) findViewById(R.id.buttonSetting);
        btnProfile = (Button) findViewById(R.id.buttonProfile);

        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(ContactFragment.newInstance(MainActivity.this));
            }
        });
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(ProfileFragment.newInstance(MainActivity.this));
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(SettingsFragment.newInstance(MainActivity.this));
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(GalleryFragment.newInstance(MainActivity.this));
            }
        });
    }

    public void changeContent(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .commitAllowingStateLoss();
    }
}
