package com.dhickcunk.modul2_praktikum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = (EditText)findViewById(R.id.editTextUsername);
        editTextPassword = (EditText)findViewById(R.id.editTextPassword);

        btnLogin = (Button)findViewById(R.id.buttonLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();

                if ( username.trim().equalsIgnoreCase("")){
                    editTextUsername.setError("Username Tidak Boleh Kosong");
                    editTextUsername.requestFocus();
                }else if(password.trim().equalsIgnoreCase("")){
                    editTextPassword.setError("Password Tidak Boleh Kosong");
                }else{
                    if (username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")){
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        LoginActivity.this.finish();
                    }else{
                        Toast.makeText(LoginActivity.this, "Salah Cuy", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
